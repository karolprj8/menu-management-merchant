<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_110159_menusmanagement extends Migration
{
	/*
    public function up()
    {

    }

    public function down()
    {
        echo "m160120_110159_menusmanagement cannot be reverted.\n";

        return false;
    }

    */
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
		$tableOptions = null;
		$tablename = 'menus';
		if ( $this->db->driverName === 'mysql' )
		{
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		// Create user table
		$this->createTable($tablename, array(
			'id'                 => 'pk',
			'root'               => 'int default null',
			'lft'                => 'int not null',
			'rgt'                => 'int not null',
			'lvl'         		 => 'smallint not null',
			'name'               => 'varchar(212) not null',
			'node_url'           => 'varchar(212) default null',
			'icon'               => 'varchar(212) default null',
			'icon_type'          => 'tinyint not null default 1',
			'active'             => 'tinyint not null default 1',
			'selected'           => 'tinyint not null default 0',
			'disabled'           => 'tinyint not null default 0',
			'readonly'           => 'tinyint not null default 0',
			'visible'            => 'tinyint not null default 1',
			'collapsed'          => 'tinyint not null default 0',
			'visible'            => 'tinyint not null default 1',
			'movable_u'          => 'tinyint not null default 1',
			'movable_d'          => 'tinyint not null default 1',
			'movable_l'          => 'tinyint not null default 1',
			'movable_r'          => 'tinyint not null default 1',
			'removable'          => 'tinyint not null default 1',
			'removable_all'      => 'tinyint not null default 1',
		), $tableOptions);
		
		$this->execute('ALTER TABLE `menus`
						  ADD KEY `menus_NK1` (`root`),
						  ADD KEY `menus_NK2` (`lft`),
						  ADD KEY `menus_NK3` (`rgt`),
						  ADD KEY `menus_NK4` (`lvl`),
						  ADD KEY `menus_NK5` (`active`)');
    }

    public function safeDown()
    {
		$tablename = 'menus';
		$this->dropTable($tablename);
		
    }
	
}
	
?>