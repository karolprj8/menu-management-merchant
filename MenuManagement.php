<?php 

namespace karolprj8\menumanagement;

use Yii;

class MenuManagement extends \yii\base\Module
{
    public function init()
    {
        parent::init();
		Yii::configure($this, require(__DIR__ . '/config/config.php'));
		Yii::setAlias('@menumanagement', dirname(__FILE__));
        
    }
}


?>