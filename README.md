**1) Composer:**

"require": {
		...
		"karolprj8/menu-management": "*",
		...
    },

"repositories": [
	{
        "type": "git",
        "url": "https://karolprj8:@bitbucket.org/karolprj8/menu-management.git"
	}
],

**2) config.php**

'modules'=>[
		.....
		'menu-management' => [
			'class' => 'karolprj8\menumanagement\MenuManagement',
			'layout' => '@app/views/layouts/XXXXXXX'
		],
		'treemanager' =>  [
			'class' => '\kartik\tree\Module'
		],
		.....
	]



**3) Migration:**

./yii migrate --migrationPath=vendor/karolprj8/menu-management/migrations/



**4) Test:** 

yourdomain/menu-management/menus/index

**5) Using:**


use karolprj8\menumanagement\models\Menus;

echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => Menus::getAsArray(1)
	]);